<?php

class etouchesclient{
	
	public $outputErrors = false;
	public $exitOnError = false;
	
	protected $client = null;
	
	public function __construct($accountid, $key) {
	      $url = variable_get('etouches_api_url', 'https://qa.eiseverywhere.com/api/');
		$this->outputErrors = $this->exitOnError = true;
		
		$this->client = new SoapClient(null, array(
			'location' => $url,
			'uri' => 'urn://etouchesapi'
		));
		
		$result = $this->client->authorize($accountid, $key);
		if(!is_bool($result)) $result = $this->converToArray($result);
		if(!empty($result['error'])) $this->outputError($result['error']);
				
		$this->outputErrors = $this->exitOnError = false;
	}
	
	public function __destruct(){
		if(is_object($this->client)) $this->client = null;
	}
	
	public function listAvailableMethods(){
		$result = $this->converToArray($this->client->listAvailableMethods());
		if(!empty($result['error'])) $this->outputError($result['error']);

		return $result;
	}
	
	public function getEventList(){
		$result = $this->converToArray($this->client->getEventList());
		if(!empty($result['error'])) $this->outputError($result['error']);
		
		return $result;
	}
	
	public function getEventDetails($eventid){
		$result = $this->converToArray($this->client->getEventDetails($eventid));
		if(!empty($result['error'])) $this->outputError($result['error']);
		
		return $result;
	}
	
	public function getAttendeeList($eventid = 0){
		$result = $this->converToArray($this->client->getAttendeeList($eventid));
		if(!empty($result['error'])) $this->outputError($result['error']);
		
		return $result;
	}
	
	public function getAttendeeDetails($attendeeid, $eventid = 0){
		$result = $this->converToArray($this->client->getAttendeeDetails($attendeeid, $eventid));
		if(!empty($result['error'])) $this->outputError($result['error']);
		
		return $result;
	}
	
	public function getReportList($eventid = 0){
		$result = $this->converToArray($this->client->getReportList($eventid));
		if(!empty($result['error'])) $this->outputError($result['error']);
		
		return $result;
	}
	
	public function getReportDetails($reportid, $eventid = 0){
		$result = $this->converToArray($this->client->getReportDetails($reportid, $eventid));
		if(!empty($result['error'])) $this->outputError($result['error']);
		
		return $result;
	}
	
	public function getQuestionList($eventid = 0){
		$result = $this->converToArray($this->client->getQuestionList($eventid));
		if(!empty($result['error'])) $this->outputError($result['error']);
		
		return $result;
	}
	
	public function getQuestionListDetailed($eventid = 0, $required = false){
		$result = $this->converToArray($this->client->getQuestionListDetailed($eventid, $required));
		if(!empty($result['error'])) $this->outputError($result['error']);
		
		return $result;
	}
	
	public function getCategoryList($eventid = 0){
		$result = $this->converToArray($this->client->getCategoryList($eventid));
		if(!empty($result['error'])) $this->outputError($result['error']);
		
		return $result;
	}
	
	public function getSessionList($eventid = 0){
		$result = $this->converToArray($this->client->getSessionList($eventid));
		if(!empty($result['error'])) $this->outputError($result['error']);
		
		return $result;
	}
	
	public function getSessionListDetailed($eventid = 0){
		$result = $this->converToArray($this->client->getSessionListDetailed($eventid));
		if(!empty($result['error'])) $this->outputError($result['error']);
		
		return $result;
	}
	
	public function getSpeakerList($sessionid = 0, $eventid = 0){
		$result = $this->converToArray($this->client->getSpeakerList($sessionid, $eventid));
		if(!empty($result['error'])) $this->outputError($result['error']);
		
		return $result;
	}
	
	public function setAttendeeCheckIn($attendeeid, $eventid = 0){
		$result = $this->converToArray($this->client->setAttendeeCheckIn($eventid, $attendeeid));
		if(!empty($result['error'])) $this->outputError($result['error']);
		
		return $result;
	}
	
	/* ---------------------------------------- */
	
	protected function converToArray($xml, $array = array()){
		if(!is_object($xml)) $xml = simplexml_load_string($xml);
		if(!is_object($xml)) return array();
		
		foreach($xml->children() as $node){
			$name = $node->getName();
			$value = (string) $node[0];
			if(!$node->children()){
				if(count($xml->xpath($name)) > 1){
					$array[$name][] = $value;
				}else{
					$array[$name] = $value;
				}
			}else{
				$array[$name][] = $this->converToArray($node, array());
			}
		}
		
		return $array;
	}
	
	protected function outputError($details){
		if($this->outputErrors){
			echo 'Error<br />';
			$error = is_array($details) ? $details : array($details);
			foreach($error as $key => $val)
				echo $val .'<br />';
			if($this->exitOnError) exit;
		}
	}
	
}

?>