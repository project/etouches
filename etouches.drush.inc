<?php

/**
 * Implementation of hook_drush_command().
 */
function etouches_drush_command() {
  return array(
    'etouches' => array(
      'callback' => 'etouches_drush_execute',
      'description' => 'implement an ETouches API function',
    ),
  );
}

/**
 * Execute an ETouches API function.
 */
function etouches_drush_execute() {
  $args = func_get_args();
  $command = array_shift($args);
  $return = etouches_execute($command, $args);
  print print_r($return, TRUE);
}